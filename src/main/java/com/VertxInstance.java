package com;

import io.vertx.core.Vertx;

public class VertxInstance {
    private static  Vertx vertx;
    public static Vertx getVertx(){
        if(vertx == null){
            vertx = Vertx.vertx();
        }
        return vertx;
    }
}
