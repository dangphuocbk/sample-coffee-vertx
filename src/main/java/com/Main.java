package com;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private final static Logger log = LogManager.getLogger("Main");

    public static void main(String[] args) {
        Vertx vertx = VertxInstance.getVertx();
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.post("/v1/io").handler(new VertxHandler());
        HttpServerOptions httpServerOptions = new HttpServerOptions();
        httpServerOptions.setPort(8181);
        vertx.createHttpServer(httpServerOptions).requestHandler(router).listen(asyncResult -> {
            if (asyncResult.failed()) {
                log.error("Http Server Start Fail", asyncResult.cause());
            } else {
                log.info("Http Server Start Success! listen port: {}", asyncResult.result().actualPort());
            }
        });
    }
}
