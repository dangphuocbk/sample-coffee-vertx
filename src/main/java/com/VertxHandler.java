package com;


import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VertxHandler implements Handler<RoutingContext> {
    private final Logger logger = LogManager.getLogger("CoffeeHandler");

    @Override
    public void handle(RoutingContext context) {
        IO();
        buildResponse(context);
    }

    private void IO() {
        logger.warn("Thread: {}", Thread.currentThread());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void buildResponse(RoutingContext context) {
        final HttpServerResponse response = context.response();
        response.setChunked(true);
        response.setStatusCode(200);
        response.write("success");
        response.end();
        response.close();
    }
}
